﻿using Microsoft.Extensions.Configuration;
using System;

namespace Common.Utils.Exceptions
{
    public class BusinessException : Exception
    {
        public BusinessException(IConfiguration Config, string message, params object[] args)
            : base(string.Format(Config.GetValue<string>("ExceptionBundle:" + message) + "~~" + message, args))
        {
            //throw new CustomException("Exception with parameter value {0}, {1}", param[0], param[1])
        }

        public BusinessException()
        {
            //throw new CustomException()
        }

        public BusinessException(IConfiguration Config, string message)
            : base(string.Format(Config.GetValue<string>("ExceptionBundle:" + message) + "~~" + message))
        {
            //throw new CustomException(message)
        }

        public BusinessException(string message)
            : base(message)
        {
            //throw new CustomException(message)
        }

        public BusinessException(string message, params object[] args)
            : base(string.Format(message, args))
        {
            //throw new CustomException("Exception with parameter value {0}, {1}", param[0], param[1])
        }

        public BusinessException(string message, Exception inner)
            : base(message, inner)
        {
            //throw new CustomException(message, innerException)
        }

    }
}