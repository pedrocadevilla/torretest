﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Common.Utils.Repository
{

    /// <summary>
    /// Interfaz que proporciona la funcionalidad bÃ¡sica 
    /// para realizar operaciones sobre las colecciones de datos
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Agregar un nuevo elemento a la colecciÃ³n
        /// </summary>
        /// <param name="entity">Elemento a guardar</param>
        void Add(TEntity entity);

        /// <summary>
        /// Agregar una colecciÃ³n de objetos a la colecciÃ³n
        /// </summary>
        /// <param name="entities">Lista de objetos a guardar</param>
        void AddRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// Obtener un elemento dado su identificador
        /// </summary>
        /// <param name="id">id del elemento</param>
        /// <returns>Objeto de la clase definida</returns>
        Task<TEntity> GetAsync(long id);

        /// <summary>
        /// Obtener un elemento dado su identificador
        /// </summary>
        /// <param name="id">id del elemento</param>
        /// <returns>Objeto de la clase definida</returns>
        IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Consultar todos los objetos de la colecciÃ³n
        /// </summary>
        /// <returns>Lista de objetos de la clase dada</returns>
        Task<IEnumerable<TEntity>> GetAllAsync();


        /// <summary>
        /// Obtener un IQueryable del tipo actual
        /// </summary>
        /// <returns>IQueryable del tipo de entidad actual</returns>
        IQueryable<TEntity> QueryAll();

        /// <summary>
        /// Buscar un elemento dentro de la colecciÃ³n
        /// </summary>
        /// <param name="predicate">lambda que define el objeto a buscar</param>
        /// <returns>Objeto de la clase dada</returns>
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Eliminar un elemento de la base de la colecciÃ³n
        /// </summary>
        /// <param name="entity">Elemento a eliminar</param>
        void Remove(TEntity entity);

        /// <summary>
        /// Eliminar una colecciÃ³n de objetos de la colecciÃ³n
        /// </summary>
        /// <param name="entities">Lista de elementos a eliminar</param>
        void RemoveRange(IEnumerable<TEntity> entities);

    }
}