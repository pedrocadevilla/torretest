﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Torre.Core.Models
{
    [Table("PERSON")]
    public partial class Person
    {
        public Person()
        {
            Assignments = new HashSet<Assignments>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public string Image { get; set; }
        [StringLength(250)]
        public string Headline { get; set; }
        [StringLength(100)]
        public string Code { get; set; }

        [InverseProperty("Person")]
        public virtual ICollection<Assignments> Assignments { get; set; }
    }
}
