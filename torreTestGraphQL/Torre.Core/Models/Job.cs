﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Torre.Core.Models
{
    [Table("JOB")]
    public partial class Job
    {
        public Job()
        {
            Assignments = new HashSet<Assignments>();
        }

        [Column("ID")]
        public long Id { get; set; }
        public string Image { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(250)]
        public string Objective { get; set; }
        [StringLength(100)]
        public string Code { get; set; }

        [InverseProperty("Job")]
        public virtual ICollection<Assignments> Assignments { get; set; }
    }
}
