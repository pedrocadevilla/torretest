﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Torre.Core.Models
{
    [Table("ASSIGNMENTS")]
    public partial class Assignments
    {
        [Column("ID")]
        public long Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Date { get; set; }
        public long? PersonId { get; set; }
        public long? JobId { get; set; }

        [ForeignKey("JobId")]
        [InverseProperty("Assignments")]
        public virtual Job Job { get; set; }
        [ForeignKey("PersonId")]
        [InverseProperty("Assignments")]
        public virtual Person Person { get; set; }
    }
}
