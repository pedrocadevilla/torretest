﻿using Common.Utils.Repository;
using System.Threading.Tasks;
using Torre.Core.Models;

namespace Torre.Persistence.Repositories
{
    public interface IJobRepository : IRepository<Job>
    {
        Task<Job> ObtenerXCodigo(string codigo);
    }
}
