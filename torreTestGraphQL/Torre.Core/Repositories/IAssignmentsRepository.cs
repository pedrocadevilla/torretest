﻿using Common.Utils.Repository;
using System.Threading.Tasks;
using Torre.Core.Models;

namespace Torre.Persistence.Repositories
{
    public interface IAssignmentsRepository : IRepository<Assignments>
    {
        Task<Assignments> ObtenerEquipoXId(long id);
    }
}
