﻿using Common.Utils.Repository;
using System.Threading.Tasks;
using Torre.Core.Models;

namespace Torre.Persistence.Repositories
{
    public interface IPersonRepository : IRepository<Person>
    {
        Task<Person> ObtenerXCodigo(string codigo);
    }
}
