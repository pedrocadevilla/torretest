﻿using Torre.Persistence.Repositories;
using System;
using System.Threading.Tasks;

namespace Torre.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IAssignmentsRepository Assignments { get; }
        IPersonRepository Person { get; }
        IJobRepository Job { get; }

        Task<int> Complete();
    }
}
