using HotChocolate;
using HotChocolate.AspNetCore;
using HotChocolate.Execution;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Torre.Core;
using Torre.Persistence;
using Torre.Api.GraphQL.Mutations;
using System.Security.Claims;
using HotChocolate.AspNetCore.Voyager;
using Torre.Api.GraphQL.MutationsType;
using Torre.Api.GraphQL.QueryType;

namespace Torre.Api
{
    /// <summary>
    /// Configura los servicios y la canalización de solicitudes de la aplicación
    /// </summary>
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        private readonly IConfiguration _configuration;
        /// <summary>
        /// Representa un conjunto de propiedades de configuración de la aplicación de clave y valor.
        /// </summary>
        /// <param name="configuration">Configuraciones</param>
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        /// <summary>
        /// Configuraciones del servicio
        /// </summary>
        /// <param name="services">Coleccion de Servicios</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddTransient<ApplicationRoleParams>();
            // Configurar base de datos            
            services.AddDbContext<TORREContext>(options =>
                options.UseSqlServer(_configuration.GetConnectionString("DefaultConnection")),
                ServiceLifetime.Transient
            );
            // Caracteristicas de Graphql
            services.AddTransient<Mutation>();
            services.AddTransient<Query>();
            services.AddTransient<Mutation>();

            // Servicios Graphql
            services.AddGraphQL(
                SchemaBuilder.New()
                .AddQueryType<QueryType>()
                .AddMutationType<MutationType>()
                .Create(), builder => builder
                .UseDefaultPipeline());
            // Agregar acceso CORS
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithExposedHeaders("Token")
                    //.AllowCredentials()
                    .Build());
            });
            // AppContext.SetSwitch("System.Net.Http.UseSocketsHttpHandler", false);
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<IConfiguration>(_configuration);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors("CorsPolicy");

            app.UseExceptionHandler(a => a.Run(async context =>
            {
                var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                var exception = exceptionHandlerPathFeature.Error;
            }));

            app.UseCors("CorsPolicy");
            app.UseGraphQL();
            app.UsePlayground();
            app.UseVoyager();

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Service running");
            });
        }
    }
}