﻿using HotChocolate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Torre.Core;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.Resolvers
{
    public class AssignmentsResolver
    {
        protected readonly IUnitOfWork _unitOfWork;

        public AssignmentsResolver(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Person> ObtenerPersonaAsync([Parent]Assignments datos)
        {
            long id = datos.PersonId ?? 0;
            return await _unitOfWork.Person.GetAsync(id);
        }

        public async Task<Job> ObtenerTrabajoAsync([Parent]Assignments datos)
        {
            long id = datos.JobId ?? 0;
            return await _unitOfWork.Job.GetAsync(id);
        }
    }
}
