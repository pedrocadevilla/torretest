﻿using System;
using System.Threading.Tasks;
using Torre.Core;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        private readonly IUnitOfWork _unitOfWork;

        public Mutation(IUnitOfWork unitOfWork)
        { 
            _unitOfWork = unitOfWork;
        }

        public async Task<Assignments> CrearEquipoTrabajoAsync(Person person, Job job)
        {
            Person persona = await _unitOfWork.Person.ObtenerXCodigo(person.Code);
            Job trabajo = await _unitOfWork.Job.ObtenerXCodigo(job.Code);
            if (persona == null) _unitOfWork.Person.Add(person);
            else person.Id = persona.Id;
            if (trabajo == null) _unitOfWork.Job.Add(job);
            else job.Id = trabajo.Id;
            Assignments asignacion = new Assignments();
            asignacion.JobId = job.Id;
            asignacion.PersonId = person.Id;
            asignacion.Date = DateTime.Now;
            _unitOfWork.Assignments.Add(asignacion);
            await _unitOfWork.Complete();
            return asignacion;
        }
    }
}
