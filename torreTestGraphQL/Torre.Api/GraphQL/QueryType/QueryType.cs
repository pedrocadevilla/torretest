﻿using HotChocolate.Types;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.QueryType
{
    public class QueryType : ObjectType<Query>
    {
        protected override void Configure(IObjectTypeDescriptor<Query> descriptor)
        {
            descriptor.Name("Consultas");
            descriptor.Description("Conjunto de elementos acerca de los cuales se puede obtener informaciòn");

            descriptor.Field(x => x.GetAllAssignments())
                .Type<ListType<AssignmentsType>>()
                .Description("Consulta todas las asignaciones creadas en el sistema");

            descriptor.Field(x => x.GetAssignment(default))
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Type<AssignmentsType>()
                .Description("Consulta una asignacion especifica dado un id de asignacion");
        }
    }
}
