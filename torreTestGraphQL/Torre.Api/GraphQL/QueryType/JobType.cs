﻿using HotChocolate.Types;
using Torre.Api.GraphQL.Resolvers;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.QueryType
{
    public class JobType : ObjectType<Job>
    {
        protected override void Configure(IObjectTypeDescriptor<Job> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Job type");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Code);
            descriptor.Field(x => x.Image);
            descriptor.Field(x => x.Name);
            descriptor.Field(x => x.Objective);
        }
    }
}
