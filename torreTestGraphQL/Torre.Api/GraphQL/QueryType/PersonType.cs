﻿using HotChocolate.Types;
using Torre.Api.GraphQL.Resolvers;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.QueryType
{
    public class PersonType : ObjectType<Person>
    {
        protected override void Configure(IObjectTypeDescriptor<Person> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Person type");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Code);
            descriptor.Field(x => x.Headline);
            descriptor.Field(x => x.Image);
            descriptor.Field(x => x.Name);
        }
    }
}
