﻿using HotChocolate.Types;
using Torre.Api.GraphQL.Resolvers;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.QueryType
{
    public class AssignmentsType : ObjectType<Assignments>
    {
        protected override void Configure(IObjectTypeDescriptor<Assignments> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("ActividadLaborales de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.PersonId);
            descriptor.Field(x => x.JobId);
            descriptor.Field(x => x.Date);
            descriptor.Field<AssignmentsResolver>(x => x.ObtenerPersonaAsync(default));
            descriptor.Field<AssignmentsResolver>(x => x.ObtenerTrabajoAsync(default));
        }
    }
}
