﻿using HotChocolate.Types;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.InputTypes
{
    public class AssignmentsInputType : InputObjectType<Assignments>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Assignments> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("AssignmentsInput");

            descriptor.Field(x => x.PersonId);
            descriptor.Field(x => x.JobId);
        }
    }
}
