﻿using HotChocolate.Types;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.InputTypes
{
    public class PersonInputType : InputObjectType<Person>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Person> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("PersonInput");

            descriptor.Field(x => x.Image);
            descriptor.Field(x => x.Name);
            descriptor.Field(x => x.Headline);
            descriptor.Field(x => x.Code);
        }
    }
}
