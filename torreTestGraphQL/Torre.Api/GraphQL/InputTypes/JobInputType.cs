﻿using HotChocolate.Types;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.InputTypes
{
    public class JobInputType : InputObjectType<Job>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Job> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("JobInput");

            descriptor.Field(x => x.Name);
            descriptor.Field(x => x.Image);
            descriptor.Field(x => x.Objective);
            descriptor.Field(x => x.Code);
        }
    }
}
