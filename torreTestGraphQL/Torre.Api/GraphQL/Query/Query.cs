﻿using HotChocolate.Types;
using Torre.Core;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.QueryType
{
    public partial class Query 
    {
        private readonly IUnitOfWork _unitOfWork;

        public Query(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
