﻿using HotChocolate.Types;
using System.Collections.Generic;
using System.Threading.Tasks;
using Torre.Core;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.QueryType
{
    public partial class Query 
    {
        /// <summary>
        /// Busca un equipo de trabajo dado un id de equipo
        /// </summary>
        /// <param name="id">Id del equipo que se desea consultar</param>
        /// <returns>Equipo de trabajo</returns>
        public async Task<Assignments> GetAssignment(long id)
        {
            return await _unitOfWork.Assignments.ObtenerEquipoXId(id);
        }
        /// <summary>
        /// Busca un equipo de trabajo dado un id de equipo
        /// </summary>
        /// <param name="id">Id del equipo que se desea consultar</param>
        /// <returns>Equipo de trabajo</returns>
        public async Task<IEnumerable<Assignments>> GetAllAssignments()
        {
            return await _unitOfWork.Assignments.GetAllAsync();
        }
    }
}
