﻿using HotChocolate.Types;
using Torre.Api.GraphQL.InputTypes;
using Torre.Api.GraphQL.Mutations;
using Torre.Api.GraphQL.QueryType;

namespace Torre.Api.GraphQL.MutationsType
{
    public class MutationType : ObjectType<Mutation>
    {
        protected override void Configure(IObjectTypeDescriptor<Mutation> descriptor)
        {
            descriptor.Name("Mutaciones");
            descriptor.Description("Operaciones para guardar, modificar y Remove registros");

            descriptor.Field(t => t.CrearEquipoTrabajoAsync(default, default))
                .Type<NonNullType<AssignmentsType>>()
                .Argument("person", a => a.Type<NonNullType<PersonInputType>>())
                .Argument("job", a => a.Type<NonNullType<JobInputType>>())
                .Description("Permite crear un nuevo equipo de trabajo. Recibe la persona a ser agregada y el trabajo al cual se asignara");
        }
    }
}
