﻿using Torre.Core;
using Torre.Persistence.Repositories;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Torre.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TORREContext _Context;
        private readonly IConfiguration _Configuration;

        public IAssignmentsRepository Assignments { get; private set; }
        public IPersonRepository Person { get; private set; }
        public IJobRepository Job { get; private set; }

        public UnitOfWork(TORREContext Context, IConfiguration Configuration)
        {
            _Context = Context;
            _Configuration = Configuration;

            Assignments = new AssignmentsRepository(_Context, _Configuration);
            Person = new PersonRepository(_Context, _Configuration);
            Job = new JobRepository(_Context, _Configuration);
        }

        public async Task<int> Complete()
        {
            return await _Context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _Context.Dispose();
        }
    }
}
