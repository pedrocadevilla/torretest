﻿using Microsoft.EntityFrameworkCore;
using Torre.Core.Models;

namespace Torre.Persistence
{
    public partial class TORREContext : DbContext
    {
        public TORREContext()
        {
        }

        public TORREContext(DbContextOptions<TORREContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Assignments> Assignments { get; set; }
        public virtual DbSet<Job> Job { get; set; }
        public virtual DbSet<Person> Person { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-HNB9D0P\\SQLEXPRESS;Database=TORRE;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Assignments>(entity =>
            {
                entity.HasOne(d => d.Job)
                    .WithMany(p => p.Assignments)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("FK_REFERENCE_2");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.Assignments)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK_REFERENCE_1");
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.Image).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.Objective).IsUnicode(false);
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.Headline).IsUnicode(false);

                entity.Property(e => e.Image).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });
        }
    }
}
