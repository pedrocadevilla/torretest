﻿using Common.Utils.Exceptions;
using Common.Utils.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Torre.Core.Models;

namespace Torre.Persistence.Repositories
{
    class AssignmentsRepository : Repository<Assignments>, IAssignmentsRepository
    {

        public AssignmentsRepository(DbContext context, IConfiguration Configuration) : base(context)
        {
            Config = Configuration;
        }

        public TORREContext TORREContext
        {
            get { return Context as TORREContext; }
        }

        public IConfiguration Config { get; private set; }

        public async Task<Assignments> ObtenerEquipoXId(long id)
        {
            Assignments equipo = await TORREContext.Assignments
            .Where(x => x.Id == id)
            .FirstOrDefaultAsync();
            if (equipo == null)
            {
                throw new BusinessException(Config, "EQUIPO-NO-ENCONTRADO");
            }
            return equipo;
        }
    }
}
