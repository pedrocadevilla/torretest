﻿using Common.Utils.Exceptions;
using Common.Utils.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Torre.Core.Models;

namespace Torre.Persistence.Repositories
{
    class PersonRepository : Repository<Person>, IPersonRepository
    {

        public PersonRepository(DbContext context, IConfiguration Configuration) : base(context)
        {
            Config = Configuration;
        }

        public TORREContext TORREContext
        {
            get { return Context as TORREContext; }
        }

        public IConfiguration Config { get; private set; }

        public async Task<Person> ObtenerXCodigo(string codigo)
        {
            Person persona = await TORREContext.Person
            .Where(x => x.Code == codigo)
            .FirstOrDefaultAsync();
            return persona;
        }
    }
}
