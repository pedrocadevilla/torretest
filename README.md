# TORRE

The current project has been developed using .NET, GraphQL, SQLServer, React-Redux and React-Toolkit.

## Definition

This app allows an user to select a job from the available job oportunities and asssign a person to this job opportunity and it would be saved to the database.

## Preparation

Download the files go to the torre/torrereact folder inside the downloaded files using the console and use the comand yarn. 

Open the GraphQL folder and open the solution you will have to change 2 files located in

1) Torre.Api/Appsettings.json line 3

```python
"DefaultConnection": "Server=YOUR_SERVER_NAME;Database=YOUR_DB_NAME;Trusted_Connection=True",
```

1) Torre.Persinstence/TORREContext.cs line 23

```python
"DefaultConnection": "Server=YOUR_SERVER_NAME;Database=YOUR_DB_NAME;Trusted_Connection=True",
```

Open SQLServer and use the script located in torreDB folder in order to create the DB 

## Usage

Using the terminal located in torre/torrereact folder run the following command and you will have a client server runing on port 3000

```python
yarn start
```

Open the solution of VS and run this usin the Torre.Api configuration and you will have the server side running

```python
Use Torre.Api
```

After this steps are completed you can use the app in the link 

```python
http://localchost:3000/
```

## Guide

The app lets you select from any job opportunities and add to this a person to be assigned to this job you can see the detail 
of every job or person and after you select the job and the person the service automatically saves this ids to the database 
after this you can do all the process again

## Considerations

The link provided (https://torrereact.azurewebsites.net/) to test the app is not fully functional because of some restrictions of Azure free server
and it couldn't be corrrectly deployed and my backend created an error and the frontEnd couldn't load correctly the styles 

To avoid using cors plugin using REACT_APP_GET_PERSON change the .env variable and put this instead https://cors-anywhere.herokuapp.com/https://torre.bio/api/bios/

The progress log and some considerations of the program are located in the file ProgressLog in the root folder 
