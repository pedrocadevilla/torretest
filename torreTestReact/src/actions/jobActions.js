const { createAsyncThunk } = require("@reduxjs/toolkit");

export const getAllOpportunities = createAsyncThunk(
    "job/getAllOpportunities",
    (offset) => {
        return fetch(process.env.REACT_APP_POST_OPPORTUNITIES + (offset-1)*4 + '&size=4&aggregate=0', {method:'POST', headers: {"Content-Type": "application/json"}})
        .then(response => {
            if(!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(json => json);
    }
)

export const getOpportunities = createAsyncThunk(
    "job/getOpportunities",
    (id) => {
        return fetch(process.env.REACT_APP_GET_OPPORTUNITIES + id)
        .then(response => {
            if(!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(json => json);
    }
)