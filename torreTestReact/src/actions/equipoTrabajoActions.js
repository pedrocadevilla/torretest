import axios from 'axios'
const { createAsyncThunk } = require("@reduxjs/toolkit");

export const createTeam = createAsyncThunk(
    "team/createTeam",
    (body) => {
        return axios({
            url: process.env.REACT_APP_VISUAL_URL,
            method: 'post',
            data: {
                query:`
                mutation crearEquipoTrabajo {
                    crearEquipoTrabajo(person: { name: "` + body.personName + `", image: "` + body.personImage + `", headline: "` + body.personHeadline + `", code: "` + body.personId + `"}, job: {  name: "` + body.jobName + `", image: "` + body.jobImage + `", objective: "` + body.jobDetail + `", code: "` + body.jobId + `" } ) {
                        id
                    }
                }`
            }
        })
        .then(response => {
            if(!response.data.data) throw Error(response.Error);
            return response.data.data.crearEquipoTrabajo;
        })
        .then(json => json);
    }
)

export const getAllAssignments = createAsyncThunk(
    "team/getAllAssignments",
    () => {
        return axios({
            url: process.env.REACT_APP_VISUAL_URL,
            method: 'post',
            data: {
                query:`
                query getAllAssignments {
                    allAssignments{
                        id,
                        personId,
                        jobId,
                        date,
                        obtenerPersona{
                            name,
                            image,
                            headline
                        },
                        obtenerTrabajo{
                            name,
                            image,
                            objective
                        }
                    }
                }`
            }
        })
        .then(response => {
            if(!response.data.data) throw Error(response.Error);
            return response.data.data.allAssignments;
        })
        .then(json => json);
    }
)
