import React from 'react';
import SelectionPage from '../components/selectionPage'
import DialogoDetalle from '../components/dialogoDetalle'
import DialogoDetallePersona from '../components/dialogoDetallePerson'
import Paginator from '../components/paginator'
import { connect } from 'react-redux'

export const Index = () => (
    <div>
        <SelectionPage/>
        <DialogoDetalle/>
        <DialogoDetallePersona/>
        <Paginator/>
    </div>
)

function mapStateToProps(state) {
    return {
        state
    };
}

export default connect(mapStateToProps)(Index);