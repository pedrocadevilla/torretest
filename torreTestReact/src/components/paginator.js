import React from 'react'
import { Grid, makeStyles } from '@material-ui/core'
import Pagination from '@material-ui/lab/Pagination'
import { connect } from "react-redux"
import { getAllOpportunities } from '../actions/jobActions'
import { getAllPersons } from '../actions/personActions'
import { changeOpportunityPage, changePersonPage } from "../slice/teamSlice"

const properStyles = makeStyles(theme => ({
    pagination: {
        margin: theme.spacing(2),
    }
}))

export const Paginator = (props) => {
    const classes = properStyles()
    return (
        <div>
            {Object.keys(props.state.persistedReducer.teamReducer.selectedJob).length === 0 &&
            <Grid container justify="center" className={classes.pagination}>
                <Pagination page={props.state.persistedReducer.teamReducer.opportunityPage}  count={Math.floor(props.state.persistedReducer.teamReducer.countJobs/4)} variant="outlined" shape="rounded" color="primary" onChange={(event, val) => props.paginatorOpportunitiesChange(val)}/>
            </Grid>
            }
            {Object.keys(props.state.persistedReducer.teamReducer.selectedJob).length > 0 &&
            <Grid container justify="center" className={classes.pagination}>
                <Pagination page={props.state.persistedReducer.teamReducer.personPage} count={Math.floor(props.state.persistedReducer.teamReducer.countPersons/4)} variant="outlined" shape="rounded" color="primary" onChange={(event, val) => props.paginatorPersonChange(val)}/>
            </Grid>
            }
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        paginatorOpportunitiesChange: (val) => {
            dispatch(changeOpportunityPage(val))
            dispatch(getAllOpportunities(val))
        },
        paginatorPersonChange: (val) => {
            dispatch(changePersonPage(val))
            dispatch(getAllPersons(val))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Paginator);
