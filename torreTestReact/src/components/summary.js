import React from 'react'
import { Grid, makeStyles, Card, CardContent } from '@material-ui/core'
import { Typography, Box, CardMedia } from '@material-ui/core/'
import { connect } from 'react-redux'
import { getOpportunities } from '../actions/jobActions'
import { returnJob } from '../slice/teamSlice'
import { getPersons } from '../actions/personActions'
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'

const properStyles = makeStyles(theme => ({
    cover: {
        maxWidth: 160,
        minHeight: 160
    },
    jobImage: {
        width: '100%',
        objectFit: 'cover'
    },
    addB: {
        color: '#00A152',
    },
    root: {
        display: 'flex',
        spacing: 2,
    },
    detail: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        maxHeight: 160,
        position: 'relative'
    },
    jobDetailContainer:{
        padding: theme.spacing(2)
    },
    cardContainer: {
        padding: 5,
    },
    buttonContainer: {
        position: 'absolute',
        bottom: 0
    }
}))

export const Summary = (props) => {
    const classes = properStyles()
    return (
        <div>
            <LoadingOverlay active={props.state.persistedReducer.teamReducer.loading} spinner={<BounceLoader/>} styles={{
                    overlay: (base) => ({
                    ...base,
                    position: 'fixed'
                    })
                }}>
                <Grid container>
                    { props.state.persistedReducer.teamReducer.assignmentsCount > 0 && Object.keys(props.state.persistedReducer.teamReducer.assignments).length > 0 &&
                        props.state.persistedReducer.teamReducer.assignments.map((assignments) => (
                        <Grid key={assignments.id} item xs={12} sm={12} md={12} lg={12} xl={12} className={classes.jobDetailContainer}>
                            <Card className={classes.root}>
                                <Card className={classes.cover}>
                                    <CardMedia
                                        component="img"
                                    className={classes.jobImage}
                                    image={assignments.obtenerTrabajo.image}
                                    />
                                </Card>
                                <div className={classes.detail}>
                                    <CardContent className={classes.cardContainer}>
                                        <Typography component="h5" variant="h5">
                                            {assignments.obtenerTrabajo.name}
                                        </Typography>
                                        <Typography variant="subtitle1" color="textSecondary">
                                            {assignments.obtenerTrabajo.objective}
                                        </Typography>
                                    </CardContent>
                                </div>
                                <Card className={classes.cover}>
                                    <CardMedia
                                        component="img"
                                    className={classes.jobImage}
                                    image={assignments.obtenerPersona.image}
                                    />
                                </Card>
                                <div className={classes.detail}>
                                    <CardContent className={classes.cardContainer}>
                                        <Typography component="h5" variant="h5">
                                            {assignments.obtenerPersona.name}
                                        </Typography>
                                        <Typography variant="subtitle1" color="textSecondary">
                                            {assignments.obtenerPersona.headline}
                                        </Typography>
                                    </CardContent>
                                </div>
                            </Card>
                        </Grid>
                        ))
                    }
                    { props.state.persistedReducer.teamReducer.assignmentsCount === 0 && Object.keys(props.state.persistedReducer.teamReducer.assignments).length === 0 &&
                        <Grid container>
                            <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                                <Box display="flex" pt={2} pr={1} pb={1}>
                                    <Typography component="h3" variant="h3">
                                        No hay trabajos asignados.
                                    </Typography>
                                </Box>
                            </Grid>
                        </Grid>
                    }
                </Grid>
            </LoadingOverlay>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        returnJob: () => {
            dispatch(returnJob())
        },
        getOpportunities: (val) => {
            dispatch(getOpportunities(val));
        },
        getPersons: (val) => {
            dispatch(getPersons(val));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Summary);