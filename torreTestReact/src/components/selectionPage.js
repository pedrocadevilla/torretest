import React from 'react'
import { Grid, makeStyles, IconButton, Card, CardContent, Button } from '@material-ui/core'
import { Typography, Box, CardMedia } from '@material-ui/core/'
import VisibilityIcon from '@material-ui/icons/Visibility'
import { connect } from 'react-redux'
import { getOpportunities } from '../actions/jobActions'
import { returnJob } from '../slice/teamSlice'
import { getPersons } from '../actions/personActions'
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'

const properStyles = makeStyles(theme => ({
    cover: {
        maxWidth: 160,
        minHeight: 160
    },
    jobImage: {
        width: '100%',
        objectFit: 'cover'
    },
    addB: {
        color: '#00A152',
    },
    root: {
        display: 'flex',
        spacing: 2,
    },
    detail: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        maxHeight: 160,
        position: 'relative'
    },
    jobDetailContainer:{
        padding: theme.spacing(2)
    },
    cardContainer: {
        padding: 5,
    },
    buttonContainer: {
        position: 'absolute',
        bottom: 0
    }
}))

export const SelectionPage = (props) => {
    const classes = properStyles()
    return (
        <div>
            <LoadingOverlay active={props.state.persistedReducer.teamReducer.loading} spinner={<BounceLoader/>} styles={{
                    overlay: (base) => ({
                    ...base,
                    position: 'fixed'
                    })
                }}>
                <Grid container>
                    { props.state.persistedReducer.teamReducer.countJobs > 0 && Object.keys(props.state.persistedReducer.teamReducer.selectedJob).length === 0 &&
                        props.state.persistedReducer.teamReducer.jobs.map((jobs) => (
                        <Grid key={jobs.id} item xs={12} sm={12} md={6} lg={6} xl={4} className={classes.jobDetailContainer}>
                            <Card className={classes.root}>
                                <Card className={classes.cover}>
                                    <CardMedia
                                        component="img"
                                    className={classes.jobImage}
                                    image={jobs.organizations[0].picture}
                                    />
                                </Card>
                                <div className={classes.detail}>
                                    <CardContent className={classes.cardContainer}>
                                        <Typography component="h5" variant="h5">
                                            {jobs.organizations.name}
                                        </Typography>
                                        <Typography variant="subtitle1" color="textSecondary">
                                            {jobs.objective}
                                        </Typography>
                                        <Typography variant="subtitle2" color="textSecondary">
                                            {jobs.type}
                                        </Typography>
                                    </CardContent>
                                    <Grid container className={classes.buttonContainer}>
                                        <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Box display="flex" flexDirection="row-reverse" pt={2} pr={1} pb={1}>
                                                <IconButton aria-label="view" onClick={(event) => props.getOpportunities(jobs.id)}>
                                                    <VisibilityIcon/>
                                                </IconButton>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Card>
                        </Grid>
                        ))
                    }
                    { props.state.persistedReducer.teamReducer.countPersons > 0 && Object.keys(props.state.persistedReducer.teamReducer.selectedJob).length > 0 &&
                        props.state.persistedReducer.teamReducer.persons.map((persons) => (
                        <Grid key={persons.subjectId} item xs={12} sm={12} md={6} lg={6} xl={4} className={classes.jobDetailContainer}>
                            <Card className={classes.root}>
                                <Card className={classes.cover}>
                                    <CardMedia
                                        component="img"
                                    className={classes.jobImage}
                                    image={persons.picture}
                                    />
                                </Card>
                                <div className={classes.detail}>
                                    <CardContent className={classes.cardContainer}>
                                        <Typography component="h5" variant="h5">
                                            {persons.name}
                                        </Typography>
                                        <Typography variant="subtitle1" color="textSecondary">
                                            {persons.professionalHeadline}
                                        </Typography>
                                        <Typography variant="subtitle2" color="textSecondary">
                                            {persons.locationName}
                                        </Typography>
                                    </CardContent>
                                    <Grid container className={classes.buttonContainer}>
                                        <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Box display="flex" flexDirection="row-reverse" pt={2} pr={1} pb={1}>
                                                <IconButton aria-label="view" onClick={(event) => props.getPersons(persons.username)}>
                                                    <VisibilityIcon/>
                                                </IconButton>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Card>
                        </Grid>
                        ))
                    }
                    { props.state.persistedReducer.teamReducer.countPersons > 0 && Object.keys(props.state.persistedReducer.teamReducer.selectedJob).length > 0 &&
                        <Grid container>
                            <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                                <Box display="flex" flexDirection="row-reverse" pt={2} pr={1} pb={1}>
                                    <Button autoFocus color="inherit" onClick={() => props.returnJob()}>
                                        Go back to select a Job
                                    </Button>
                                </Box>
                            </Grid>
                        </Grid>
                    }
                </Grid>
            </LoadingOverlay>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        returnJob: () => {
            dispatch(returnJob())
        },
        getOpportunities: (val) => {
            dispatch(getOpportunities(val));
        },
        getPersons: (val) => {
            dispatch(getPersons(val));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectionPage);