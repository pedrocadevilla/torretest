import React from 'react'
import { makeStyles, Grid, Button } from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import { connect } from 'react-redux'
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied'
import { setInitialState, goToIndex } from '../slice/teamSlice'

const properStyles = makeStyles(theme => ({
    centerText: {
        textAlign: 'center'
    },
    iconBig: {
        fontSize: '100px'
    }
}))

export const ThanksText = (props) => {
    const classes = properStyles()
    return (
        <div className={classes.centerText}>
            <SentimentVerySatisfiedIcon className={classes.iconBig}/>
            <Typography variant="h3" paragraph>
                Thank for your using the system
            </Typography>
            <Typography variant="subtitle1" gutterBottom>
                You have successfully assigned { props.state.persistedReducer.teamReducer.personDetail.person.name } to the { props.state.persistedReducer.teamReducer.selectedJob.objective } job opportunity
            </Typography>
            <Grid container justify="center">
                <Button autoFocus color="inherit" onClick={() => props.setInitialState()}>
                    Start Again
                </Button>
            </Grid>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        setInitialState: async () => {
            await dispatch(setInitialState())
            dispatch(goToIndex())
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ThanksText);