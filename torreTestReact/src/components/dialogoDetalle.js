import React from 'react'
import { makeStyles, CardMedia, Typography, Grid, Button, Divider, Card, Box } from '@material-ui/core'
import { connect } from 'react-redux'
import DialogTitle from '@material-ui/core/DialogTitle'
import Dialog from '@material-ui/core/Dialog'
import Slide from '@material-ui/core/Slide'
import { closeJobDialog, selectJob } from '../slice/teamSlice'

const properStyles = makeStyles((theme) => ({
    offset: theme.mixins.toolbar,
    cover: {
        maxWidth: '90%',
        marginLeft: '5%',
        marginTop: '5%',
    },
    jobImage: {
        width: '100%',
        objectFit: 'cover'
    }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export const DialogoDetalle = (props) => {
    const classes = properStyles();
    return (
        <div>
        { Object.keys(props.state.persistedReducer.teamReducer.jobDetail).length > 0 &&
            <Dialog open={props.state.persistedReducer.teamReducer.jobDialog} TransitionComponent={Transition} maxWidth="lg">
                <DialogTitle id="simple-dialog-title">{props.state.persistedReducer.teamReducer?.jobDetail?.organizations[0]?.name} - {props.state.persistedReducer.teamReducer.jobDetail?.objective}</DialogTitle>
                <Divider />
                <Grid container>
                    <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Card className={classes.cover}>
                            <CardMedia
                                component="img"
                                className={classes.jobImage}
                                image={props.state.persistedReducer.teamReducer?.jobDetail?.organizations[0]?.picture}
                            />
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Typography variant="subtitle1" gutterBottom>
                            A little about us
                        </Typography>
                        <Divider />
                        {props.state.persistedReducer.teamReducer.jobDetail?.details?.map((details) => (
                        <Typography key={details.code} variant="subtitle2" gutterBottom>
                            {details.content}
                        </Typography>
                        ))}
                        <Divider />
                        <Typography variant="subtitle1" gutterBottom>
                        Skills needed
                        </Typography>
                        <Divider />
                        {props.state.persistedReducer.teamReducer.jobDetail?.strengths?.map((skills) => (
                        <Typography key={skills.id} variant="subtitle2" gutterBottom>
                            {skills.name}
                        </Typography>
                        ))}
                        <Divider />
                        <Typography variant="subtitle1" gutterBottom>
                            We offer
                        </Typography>
                        <Divider />
                        <Typography variant="subtitle2" gutterBottom>
                            Minimum salary: {props.state.persistedReducer.teamReducer.jobDetail?.compensation?.minAmount}
                        </Typography>
                        <Typography variant="subtitle2" gutterBottom>
                            Maximum salary: {props.state.persistedReducer.teamReducer.jobDetail?.compensation?.maxAmount}
                        </Typography>
                        <Typography variant="subtitle2" gutterBottom>
                            Contemplated {props.state.persistedReducer.teamReducer.jobDetail?.compensation?.periodicity}
                        </Typography>
                        <Typography variant="h6">
                            {props.state.persistedReducer.teamReducer.jobDetail?.organizations?.name}
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
                        <Box display="flex" flexDirection="row-reverse" pt={2} pr={1} pb={1}>
                            <Button autoFocus color="inherit" onClick={() => props.closeJobDialog()}>
                                Close
                            </Button>
                        </Box>
                    </Grid>
                    <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
                        <Box display="flex" pt={2} pr={1} pb={1}>
                            <Button autoFocus color="inherit" onClick={() => props.selectJob()}>
                                Select Job
                            </Button>
                        </Box>
                    </Grid>
                </Grid>
            </Dialog>
        }
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        closeJobDialog: () => {
            dispatch(closeJobDialog())
        },
        selectJob: () => {
            dispatch(selectJob());

        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DialogoDetalle);