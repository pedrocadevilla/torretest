import React from 'react'
import { makeStyles, CardMedia, Typography, Grid, Button, Divider, Card, Box } from '@material-ui/core'
import { connect } from 'react-redux'
import DialogTitle from '@material-ui/core/DialogTitle'
import Dialog from '@material-ui/core/Dialog'
import Slide from '@material-ui/core/Slide'
import { closePersonDialog, goToThanks } from '../slice/teamSlice'
import { createTeam } from '../actions/equipoTrabajoActions'

const properStyles = makeStyles((theme) => ({
    offset: theme.mixins.toolbar,
    cover: {
        maxWidth: '90%',
        marginLeft: '5%',
        marginTop: '5%',
    },
    jobImage: {
        width: '100%',
        objectFit: 'cover'
    }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export const DialogoDetallePerson = (props) => {
    const classes = properStyles();
    return (
        <div>
        { Object.keys(props.state.persistedReducer.teamReducer.personDetail).length > 0 &&
            <Dialog open={props.state.persistedReducer.teamReducer.personDialog} TransitionComponent={Transition} maxWidth="lg">
                <DialogTitle id="simple-dialog-title">{props.state.persistedReducer.teamReducer.personDetail?.person?.name} - {props.state.persistedReducer.teamReducer.personDetail?.person?.professionalHeadline}</DialogTitle>
                <Divider />
                <Grid container className={classes.buttonContainer}>
                    <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Card className={classes.cover}>
                            <CardMedia
                                component="img"
                                className={classes.jobImage}
                                image={props.state.persistedReducer.teamReducer.personDetail?.person?.picture}
                            />
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Typography variant="subtitle1" gutterBottom>
                            Location
                        </Typography>
                        <Divider />
                        <Typography variant="subtitle2" gutterBottom>
                            {props.state.persistedReducer.teamReducer.personDetail?.person?.location?.name}
                        </Typography>
                        <Divider />
                        <Typography variant="subtitle1" gutterBottom>
                            Languages
                        </Typography>
                        <Divider />
                        {props.state.persistedReducer.teamReducer.personDetail?.languages?.map((Languages) => (
                        <Typography  key={Languages.code} variant="subtitle2" gutterBottom>
                            {Languages.language}
                        </Typography>
                        ))}
                        <Typography variant="subtitle1" gutterBottom>
                            Links
                        </Typography>
                        <Divider />
                        {props.state.persistedReducer.teamReducer.personDetail?.person?.links?.map((links) => (
                        <Typography  key={links.code} variant="subtitle2" gutterBottom>
                            {links.name} - {links.address}
                        </Typography>
                        ))}
                    </Grid>
                </Grid>
                <Grid container className={classes.buttonContainer}>
                    <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
                        <Box display="flex" flexDirection="row-reverse" pt={2} pr={1} pb={1}>
                            <Button autoFocus color="inherit" onClick={() => props.closePersonDialog()}>
                                Close
                            </Button>
                        </Box>
                    </Grid>
                    <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
                        <Box display="flex" pt={2} pr={1} pb={1}>
                            <Button autoFocus color="inherit" onClick={() => props.createTeam(props.state.persistedReducer.teamReducer)}>
                                Select Person
                            </Button>
                        </Box>
                    </Grid>
                </Grid>
            </Dialog>
        }
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        closePersonDialog: () => {
            dispatch(closePersonDialog())
        },
        createTeam: async (store) => {
            let obj = {};
            obj.personId = store.personDetail.person.publicId;
            obj.personName = store.personDetail.person.name;
            obj.personImage = store.personDetail.person.picture;
            obj.personHeadline = store.personDetail.person.professionalHeadline;
            obj.jobId = store.jobDetail.id;
            obj.jobName = store.jobDetail.organizations[0]?.name;
            obj.jobImage = store.jobDetail.organizations[0]?.picture;
            obj.jobDetail = store.jobDetail.objective;
            await dispatch(createTeam(obj));
            dispatch(goToThanks());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DialogoDetallePerson);