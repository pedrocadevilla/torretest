import React from 'react'
import { AppBar, makeStyles, Toolbar } from '@material-ui/core'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'

const properStyles = makeStyles(theme => ({
    offset: theme.mixins.toolbar,
    menuButton: {
        marginRight: theme.spacing(2),
        color: '#FFFF',
    },
    title:{
        color: '#FFFF',
        textDecoration: 'none'
    },
    grow: {
        flexGrow: 1,
    },
    color: {
        color: '#FFFF'
    }
}))

export const NavBar = (props) => {
    const classes = properStyles()
    return (
        <div>
            <AppBar position="fixed" color="primary">
                <Toolbar>
                    <NavLink to="/" className="MuiTypography-root makeStyles-title-3 MuiTypography-h6">
                        Persons Assignment
                    </NavLink>
                    <div className={classes.grow} />
                    <NavLink to="/savedTeams" className="MuiButtonBase-root MuiButton-root MuiButton-text makeStyles-menuButton-2">
                        Assigments
                    </NavLink>
                    <NavLink to="/contact" className="MuiButtonBase-root MuiButton-root MuiButton-text makeStyles-menuButton-2">
                        Contact
                    </NavLink>
                    <NavLink to="/about" className="MuiButtonBase-root MuiButton-root MuiButton-text makeStyles-menuButton-2">
                        About
                    </NavLink>
                </Toolbar>
            </AppBar>
            <div className={classes.offset}></div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

export default connect(mapStateToProps)(NavBar);