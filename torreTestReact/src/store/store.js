import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { compose, combineReducers } from 'redux';
import teamReducer from '../slice/teamSlice'
import storage from 'redux-persist/lib/storage'
import { persistReducer, FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER } from 'redux-persist'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'

function loggerMiddleware(store){
    return function(next){
        return function(action){
            return next(action);
        }
    }
}

const persistConfig = {
    key: 'root',
    version: 1,
    storage,
    stateReconciler: autoMergeLevel2
};

const reducer = combineReducers({
    teamReducer
})

const persistedReducer = persistReducer(persistConfig, reducer)

const store = configureStore({
    reducer: {
        persistedReducer
    },
    middleware: [...getDefaultMiddleware({
        serializableCheck: {
            ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
        }
    }), loggerMiddleware],
    enhancers: [compose],
})

export default store