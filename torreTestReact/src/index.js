import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App.jsx'
import store from './store/store'
import { Provider } from 'react-redux'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import { getAllOpportunities } from './actions/jobActions'
import { getAllPersons } from './actions/personActions'
import { getAllAssignments } from './actions/equipoTrabajoActions'

let persistor = persistStore(store, {}, () => {
  store.dispatch(getAllOpportunities(1))
  store.dispatch(getAllPersons(1))
  store.dispatch(getAllAssignments())
})

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById('root')
);