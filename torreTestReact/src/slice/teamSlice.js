import { createSlice } from '@reduxjs/toolkit'
import { getAllOpportunities, getOpportunities } from '../actions/jobActions'
import { getAllPersons, getPersons } from '../actions/personActions'
import { getAllAssignments } from '../actions/equipoTrabajoActions'
import { createTeam } from '../actions/equipoTrabajoActions'
import history from '../history'

const teamSlice = createSlice({
    name: "teamReducer",
    initialState: {
        jobs: [],
        selectedJob: {},
        selectedPerson: {},
        persons: [],
        assignments: [],
        jobDetail: {},
        personDetail: {},
        loading: true,
        jobDialog: false,
        personDialog: false,
        opportunityPage: 1,
        personPage: 1,
        countJobs: 0,
        countPersons: 0,
        assignmentsCount: 0
    },
    reducers: {
        goToThanks: (state) => {
            history.push('/thanks');
        },
        setInitialState: (state) => {
            state.jobs = [];
            state.selectedJob = {};
            state.selectedPerson = {};
            state.persons = [];
            state.assignments = [];
            state.jobDetail = {};
            state.personDetail = {};
            state.loading = true;
            state.jobDialog = false;
            state.personDialog = false;
            state.opportunityPage = 1;
            state.personPage = 1;
            state.countJobs = 0;
            state.countPersons = 0;
            state.assignmentsCount = 0;
        },
        goToIndex: (state) => {
            history.push('/');
        },
        closeJobDialog: (state) => {
            state.jobDialog = false;
        },
        closePersonDialog: (state) => {
            state.personDialog = false;
        },
        changeOpportunityPage: (state, val) => {
            state.opportunityPage = val.payload;
        },
        changePersonPage: (state, val) => {
            state.personPage = val.payload;
        },
        selectJob: (state) => {
            state.selectedJob = state.jobDetail;
            state.jobDialog = false;
        },
        returnJob: (state) => {
            state.selectedJob = {};
        }
    },
    extraReducers: {
        [getAllOpportunities.pending]: state => {
            state.loading = true
        },
        [getAllOpportunities.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error;
        },
        [getAllOpportunities.fulfilled]: (state, action) => {
            state.loading = false;
            state.jobs = action.payload.results;
            state.countJobs = action.payload.total;
        },
        [getAllAssignments.pending]: state => {
            state.loading = true
        },
        [getAllAssignments.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error;
        },
        [getAllAssignments.fulfilled]: (state, action) => {
            state.loading = false;
            state.assignments = action.payload;
            state.assignmentsCount = action.payload.length;
        },
        [getAllPersons.pending]: state => {
            state.loading = true
        },
        [getAllPersons.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error;
        },
        [getAllPersons.fulfilled]: (state, action) => {
            state.loading = false;
            state.persons = action.payload.results;
            state.countPersons = action.payload.total;
        },
        [getOpportunities.pending]: state => {
            state.loading = true
        },
        [getOpportunities.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error;
        },
        [getOpportunities.fulfilled]: (state, action) => {
            state.loading = false;
            state.jobDialog = true;
            state.jobDetail = action.payload;
        },
        [getPersons.pending]: state => {
            state.loading = true
        },
        [getPersons.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error;
        },
        [getPersons.fulfilled]: (state, action) => {
            state.loading = false;
            state.personDialog = true;
            state.personDetail = action.payload;
        },
        [createTeam.pending]: state => {
            state.loading = true;
        },
        [createTeam.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error;
        },
        [createTeam.fulfilled]: (state) => {
            state.loading = false;
            state.personDialog = false;
        }
    }
})

export const { goToIndex, closeJobDialog, selectJob, changePersonPage, changeOpportunityPage, closePersonDialog, setInitialState, goToThanks, returnJob } = teamSlice.actions
export default teamSlice.reducer